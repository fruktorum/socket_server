class Actions
	@@locales = {}

	def self.logger
		SocketServer.logger
	end

	def logger
		SocketServer.logger
	end

	def text_to client, symbol, scope = @@locales[ self.class ]
		client.message :system, :message, { message: I18n.t( symbol, scope: scope, locale: client.locale ) }
		false
	end

	private

	def crypt_only client, data
		logger.debug :checking_crypt_only
		return true if client.session_key
		text_to client, :crypt_only, :common
	end
end

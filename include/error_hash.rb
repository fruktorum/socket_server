class ErrorHash < Hash
	def add key, value, options = {}
		self[ key ] ||= []
		self[ key ] << value
	end
end

require 'bundler/setup'

Bundler.require

require 'logger'
require 'openssl'
require 'open-uri'
require_relative 'lib/diffie_hellman/diffie_hellman'

class SocketServer < EM::Connection
	Root = File.expand_path( '..', __FILE__ ).freeze

	include EM::Protocols::LineProtocol

	actions = Psych.load open( File.dirname( __FILE__ ) + '/config/routes.yml', 'rb' )
	ReceivedActions = actions[ 'received_actions' ]
	SendActions = actions[ 'send_actions' ]

	@@config = {}

	class << self
		def config
			@@config
		end

		def logger
			@@logger
		end
	end

	def logger
		@@logger
	end

	def post_init
		logger.debug :client_connected

		# Filters without the services
		@unbind_callbacks = []           # User logout callbacks
		@before_send_data_changings = [] # Change data before sending
		@before_send_filters = []        # Filters before data sending
		@after_received_filters = []     # Filters after data receiving
		@after_send_callbacks = []       # Callbacks after data sending

		@attributes = {}

		@locale = 'en'
	end

	def receive_line line
		logger.debug received: line

		if ( route = ReceivedActions[ line[ 0 ].ord ] ) && ( service = ActionSpace[ route[ 'module_name' ] ] )
			logger.debug route: route, service: [ service.class, service.object_id ]

			action = route[ 'actions' ][ line[ 1 ].ord ]
			data = Base64.strict_decode64 line[ 2 .. -1 ]

			if @after_received_filters.all?{ |filter| filter.call self, data }
				validator_object = service.json_validators.find{ |validator| validator[ :action ].to_s == action }

				logger.debug json_data: data, validator_object: validator_object

				if !validator_object || validator_object[ :validator ].call( data )
					data = JSON.parse data
					logger.debug json_data: data

					filters = service.before_filters.select{ |filter| ( filter[ :on ].empty? || filter[ :on ].include?( action ) ) && ( filter[ :except ].empty? || !filter[ :except ].include?( action ) ) }
					if filters.all?{ |filter| service.send filter[ :action ], self, data }
						service.public_send action, self, data

						filters = service.after_filters.select{ |filter| filter[ :on ].include?( action ) && !filter[ :except ].include?( action ) }
						filters.each{ |filter| service.send filter[ :action ], self, data }
					else
						logger.debug :before_filters_failed
					end
				else
					r = { validator_object: validator_object }
					r[ :validator ] = validator_object[ :validator ] if validator_object
					logger.debug validator_failed: r
				end
			else
				logger.debug after_filters_failed: @after_received_filters.map{ |filter| filter.call self, data }
			end
		else
			logger.debug routing_failed: { route: route, service: service }
		end
	rescue => error
		logger.debug error_data: data
		puts $!.backtrace
		message :system, :message, message: error.to_s
	end

	def message service, action, data = {}
		if ( route = SendActions[ service.to_s ] ) && ( action_value = route[ 'actions' ][ action.to_s ] )
			if @before_send_filters.all?{ |filter| filter.call self, data }
				@before_send_data_changings.each{ |action| data = action_value.call self, data }
				logger.debug sending_data: { route: route[ 'service_command' ], action_value: action_value, data: data }

				data = Base64.strict_encode64 data.to_json

				logger.debug sending: data

				send_data "#{ route[ 'service_command' ] }#{ action_value }#{ data }\n"
				@after_send_callbacks.each{ |callback| callback.call self, sending }
			end
		else
			raise ArgumentError, "Route or action not defined (route: #{ route.inspect }, action: #{ action.inspect }, service: #{ service.inspect }, requested_action: #{ action_value.inspect })"
		end
		false
	end

	def unbind_callback action
		@unbind_callbacks << action
	end

	def before_send_filter action
		@before_send_filters << action
	end

	def after_received_filter action
		@after_received_filters << action
	end

	def after_send_callback action
		@after_send_callbacks << action
	end

	def before_send_change_data action
		@before_send_data_changings << action
	end

	def unbind
		@unbind_callbacks.each{ |action| action.call self }
		logger.debug disconnected: id
	end
end

require_relative 'config/loader'

`mkdir -p #{ File.expand_path '..', SocketServer.config[ :pidfile ] }`

at_exit{ File.delete SocketServer.config[ :pidfile ] if File.exists?( SocketServer.config[ :pidfile ] ) && Process.pid == open( SocketServer.config[ :pidfile ], 'rb', &:read ).to_i }
Process.kill 'QUIT', open( SocketServer.config[ :pidfile ], 'rb', &:read ).to_i if File.exists? SocketServer.config[ :pidfile ]

EM.run{
	host, port = [ :host, :port ].map{ |arg| SocketServer.config[ arg ] }

	EM.start_server host, port, SocketServer
	open( SocketServer.config[ :pidfile ], 'wb' ){ |file| file << Process.pid }

	logger.debug server_started: { host: host, port: port }
}

class SocketServer < EM::Connection
	attr_accessor :diffie_hellmans, :chat_room, :session_key, :login, :id, :locale

	def self.send_push sql, message, category, custom_attributes
		if config[ :environment ] !~ /development|test/
			if sql.kind_of? Array
				tokens = Postgres.exec( sql[ 0 ], sql[ 1 .. -1 ] ).to_a
			else
				tokens = Postgres.exec( sql ).to_a
			end

			Thread.new{
				tokens.select{ |token| token[ 'os' ] == 'ios' }.each{|token|
					notification = Grocer::Notification.new(
						device_token: token,
						alert: message,
						badge: 1,
						expiry: Time.now + 60 * 60,
						category: category,
						custom: custom_attributes
					)

					SocketServer::IosPushCenter.push notification
					logger.debug :succeed
				}
			}
		end
	end
end

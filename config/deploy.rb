set :stages, %w[ staging production ]

set :scm, :git
set :tests, []

set :linked_files, %w[ config/config.yml ]

namespace :deploy do
	task :bundle_update do
	end

	task :restart do
	end

	task :create_config do
	end

	after :published, 'deploy:bundle_update'
	after :finishing, 'deploy:cleanup'
	after :finished, 'deploy:restart'
end

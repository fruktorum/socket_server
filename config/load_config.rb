class SocketServer < EM::Connection
	config[ :environment ] = ARGV[ 0 ] || 'development'

	config.merge! Psych.load_file( File.expand_path '../config.yml', __FILE__ )[ config[ :environment ] ]
	config[ 'rails_core' ] = File.expand_path config[ 'rails_core' ].gsub( /\:socket_root/, SocketServer::Root )
	config.merge! Psych.load_file( File.expand_path 'config/config.yml', config[ 'rails_core' ] )[ config[ :environment ] ]

	symbolize_keys_deep = -> ( data ) {
		data.keys.each{|key|
			sym = key.to_sym
			data[ sym ] = data.delete key
			data[ sym ] = data[ sym ].gsub /\:socket_root/, File.expand_path( SocketServer::Root ) if data[ sym ].kind_of? String
			symbolize_keys_deep.call data[ sym ] if data[ sym ].kind_of? Hash
		}
	}
	symbolize_keys_deep.call config

	config[ :pidfile ] ||= 'tmp/server.pid'

	change_string = -> ( string ) {
		if string.kind_of? String
			string.gsub! /\:rails_root/, config[ :attachments ]
			string.gsub! /\:rails_core/, config[ :rails_core ]
		end
	}
	config.each_value{|type|
		if type.kind_of? Hash
			type.each_value{ |path| change_string.call path }
		else
			change_string.call type
		end
	}

	ios_push_config = {
	}
	SocketServer::IosPushCenter = Grocer.pusher ios_push_config

	@@logger = ::Logger.new config[ :environment ] == 'development' ? STDOUT : config[ :logger ]
end

options = Paperclip::Attachment.default_options
[ :storage, :default_url, :path, :convert_options ].each{ |option| options[ option ] = SocketServer.config[ :paperclip ][ option ] }
options.merge! SocketServer.config[ :paperclip ][ :restricted ]
Paperclip::Attachment.default_options[ :use_timestamp ] = false

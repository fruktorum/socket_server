class String
	def to_datetime
		DateTime.strptime self, '%Y-%m-%d %H:%M:%S.%N'
	end

	def to_time
		to_datetime.to_time
	end
end

class DateTime
	def to_s
		strftime( '%Y-%m-%d %H:%M:%S.%N' ).gsub( /(\..{3}).*/ ){ $1 }
	end
end

class Time
	def to_s
		strftime( '%Y-%m-%d %H:%M:%S.%N' ).gsub( /(\..{3}).*/ ){ $1 }
	end
end

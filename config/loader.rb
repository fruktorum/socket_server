require_relative 'load_config'
require_relative 'overloads'

ActionSpace = {}

dbconfig = Psych.load_file( SocketServer.config[ :dbconfig ] )[ SocketServer.config[ :environment ] ]

Postgres = PG.connect host: dbconfig[ 'host' ], port: dbconfig[ 'port' ], dbname: dbconfig[ 'database' ], user: dbconfig[ 'username' ], password: dbconfig[ 'password' ]

I18n.load_path = Dir[ File.expand_path( '../locales/*/*.yml', __FILE__ ) ]

Dir[ File.expand_path( '../../include/**/*.rb', __FILE__ ) ].sort_by{ |e| e.scan( /[\\\/]/ ).count }.each{ |rb_file| require_relative rb_file }

Dir[ File.expand_path( '../../actions/**/*.rb', __FILE__ ) ].each{|action|
	const_name = action[ /(?<=actions[\\\/]).+?(?=\.rb)/ ]
	const_name.scan( /[\\\/]?(.+)/ ).each{|part|
		stringify_classes = part[ 0 ].gsub( /[\\\/]/, '::' ).gsub( /([^\:]+)/ ){ $1.capitalize }

		previous = []

		stringify_classes.split( '::' ).each{|name|
			previous << name
			eval <<-EOF
				class Actions::#{ previous.join '::' } < Actions
					@@locales[ self ] = to_s.downcase[ /(?<=\\Aactions\\:\\:).+/ ].gsub '::', '.'

					@@before_filters = []
					@@after_filters = []
					@@json_validators = []
					def self.before_filter name, options = {}
						@@before_filters << { action: name.to_s, on: [ options[ :on ] ].flatten.compact.map( &:to_s ), except: [ options[ :except ] ].flatten.compact.map( &:to_s ) }
					end
					def self.after_filter name, options = {}
						@@after_filters << { action: name.to_s, on: [ options[ :on ] ].flatten.compact.map( &:to_s ), except: [ options[ :except ] ].flatten.compact.map( &:to_s ) }
					end
					def self.validate_json name, schema
						@@json_validators << { action: name.to_s, validator: -> ( json ) { JSON::Validator.validate! schema, json } }
					end
					def before_filters
						@@before_filters
					end
					def after_filters
						@@after_filters
					end
					def json_validators
						@@json_validators
					end
				end
			EOF
		}
	}

	require_relative action

	class_value = Actions.const_get const_name.gsub( /[\\\/]/, '::' ).gsub( /([^\:\:]+)/ ){ $1.capitalize }
	ActionSpace[ const_name.gsub('/', '_') ] = class_value.new
}

ActionSpace.freeze

require_relative 'custom_attributes'
